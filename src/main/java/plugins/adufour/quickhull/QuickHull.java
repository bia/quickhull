package plugins.adufour.quickhull;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Main class of the QuickHull library for Icy
 * 
 * {@link QuickHull2D}
 * {@link QuickHull3D}
 * 
 * @author Alexandre Dufour
 *
 */
public class QuickHull extends Plugin implements PluginLibrary
{   
    
}
